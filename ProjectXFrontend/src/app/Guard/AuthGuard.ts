import { Injectable } from '@angular/core';
import { Router, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanLoad, Route } from '@angular/router';
import { AuthenticateStateService } from '../Services/Authenticate/authenticate-state.service';

@Injectable({ providedIn: 'root' })
export class AuthGuard implements CanActivate {

    constructor(private router: Router, private state: AuthenticateStateService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        if (this.state.user.value === null) {
            return false;
        }

        else if (!this.state.user.value.emailVerified) {
            this.router.navigate([''], { queryParams: { returnUrl: state.url }});
            return false;
        }

        else if (this.state.user.value !== null) {
            return true;
        }

        // not logged in so redirect to login page with the return url
        this.router.navigate(['authenticate/login'], { queryParams: { returnUrl: state.url }});
        return false;
    }
}

