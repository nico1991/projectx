import { Injectable } from '@angular/core';
import { Router, CanLoad, Route } from '@angular/router';
import { AuthenticateStateService } from '../Services/Authenticate/authenticate-state.service';

@Injectable({ providedIn: 'root' })
export class AuthGuardCanLoad implements CanLoad {
  constructor(private router: Router, private state: AuthenticateStateService) {
  }
  canLoad(route: Route): boolean {
    if (this.state.user.value === null) {
        this.router.navigate([''], { queryParams: { returnUrl: route.path }});
        return false;
    } else if (!this.state.user.value.emailVerified) {
        this.router.navigate([''], { queryParams: { returnUrl: route.path }});
        return false;
    } else if (this.state.user.value !== null) {
        return true;
    }

    // not logged in so redirect to login page with the return url
    this.router.navigate(['authenticate/login'], { queryParams: { returnUrl: route.path }});
    return false;
}
}