import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { Router } from '@angular/router';
import { AuthenticateServiceService } from 'src/app/Services/Authenticate/authenticate-service.service';
import { AuthenticateStateService } from 'src/app/Services/Authenticate/authenticate-state.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {
  EmailFormGroup: FormGroup;
  PasswordFormGroup: FormGroup;
  errormessage: string;
  hide = true;
  forgotSend = false;
  verify = false;
  SendedEmail: any;
  constructor(
    private _formBuilder: FormBuilder,
    private authService: AuthenticateServiceService,
    private authState: AuthenticateStateService,
    private router: Router) {
    this.EmailFormGroup = _formBuilder.group({
      hideRequired: false,
      floatLabel: 'auto',
        // tslint:disable-next-line:max-line-length
        EmailCtrl: ['', [Validators.required , Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]]
    });

    this.PasswordFormGroup = this._formBuilder.group({
      hideRequired: false,
      floatLabel: 'auto',
      PasswordCtrl: ['', Validators.compose(  [Validators.required])]
    });
  }

  ngOnInit() {
    this.authService.afAuth.authState.subscribe(res => {
      if (res) {
        this.verify = !res.emailVerified;
      }
    });
  }

  forgotPassword() {
    if (this.EmailFormGroup.valid) {
      this.authService.fortgotPassword(this.EmailFormGroup.value.EmailCtrl);
      this.SendedEmail = this.EmailFormGroup.value.EmailCtrl;
      this.forgotSend = true;
      this.errormessage = null;
    } else {
      this.errormessage = 'Not a valid Email';
    }

  }

  onLogin() {
    this.authService.logout();
    this.authService.login(this.EmailFormGroup.value.EmailCtrl, this.PasswordFormGroup.value.PasswordCtrl)
    .catch( error => {
      this.errormessage = error.message;
    })
    .then(res => {
      if (res) {
        this.authService.refreshUser();
        if (res.user.emailVerified) {
          this.errormessage = null;
          this.router.navigate(['home']);
        } else {
          this.verify = true;
        }
      }
    });
  }
}
