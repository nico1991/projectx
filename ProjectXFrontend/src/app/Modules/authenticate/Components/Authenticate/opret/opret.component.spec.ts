import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { OpretComponent } from './opret.component';

describe('OpretComponent', () => {
  let component: OpretComponent;
  let fixture: ComponentFixture<OpretComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ OpretComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(OpretComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
