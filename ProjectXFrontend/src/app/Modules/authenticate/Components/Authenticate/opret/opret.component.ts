import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators, FormBuilder } from '@angular/forms';

import { Router } from '@angular/router';
import { AuthenticateServiceService } from 'src/app/Services/Authenticate/authenticate-service.service';
import { AuthenticateStateService } from 'src/app/Services/Authenticate/authenticate-state.service';



@Component({
  selector: 'app-opret',
  templateUrl: './opret.component.html',
  styleUrls: ['./opret.component.sass']
})
export class OpretComponent implements OnInit {
  EmailFormGroup: FormGroup;
  PasswordAgainFormGroup: FormGroup;
  PasswordFormGroup: FormGroup;
  hide = true;
  created = false;
  constructor(
    private _formBuilder: FormBuilder,
    private authService: AuthenticateServiceService,
    private router: Router,
    private authstate: AuthenticateStateService) {
    this.EmailFormGroup = _formBuilder.group({
      hideRequired: false,
      floatLabel: 'auto',
        // tslint:disable-next-line:max-line-length
        EmailCtrl: ['', [Validators.required , Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]]
    });

    this.PasswordAgainFormGroup = this._formBuilder.group({
      hideRequired: false,
      floatLabel: 'auto',
      PasswordAgainCtrl: ['', Validators.required, ]
    });

    this.PasswordFormGroup = this._formBuilder.group({
      hideRequired: false,
      floatLabel: 'auto',
// tslint:disable-next-line: max-line-length
      PasswordCtrl: ['', Validators.compose(  [Validators.required , Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')])]
    });
  }

  ngOnInit() {
  }

  match() {
    return this.PasswordFormGroup.value.PasswordCtrl === this.PasswordAgainFormGroup.value.PasswordAgainCtrl;
  }


  onCreate() {
    this.authService.signUp(this.EmailFormGroup.value.EmailCtrl, this.PasswordFormGroup.value.PasswordCtrl)
    .then(signed => {
      this.authService.sendVerification(signed.user);
      this.created = true;
      this.router.navigate(['authenticate/login']);
    });
  }
}
