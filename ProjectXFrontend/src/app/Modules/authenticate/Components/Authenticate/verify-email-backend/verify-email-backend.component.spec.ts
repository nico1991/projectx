import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VerifyEmailBackendComponent } from './verify-email-backend.component';

describe('VerifyEmailBackendComponent', () => {
  let component: VerifyEmailBackendComponent;
  let fixture: ComponentFixture<VerifyEmailBackendComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VerifyEmailBackendComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VerifyEmailBackendComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
