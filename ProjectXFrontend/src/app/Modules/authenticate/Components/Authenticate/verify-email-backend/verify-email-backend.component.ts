import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AuthenticateServiceService } from 'src/app/Services/Authenticate/authenticate-service.service';

@Component({
  selector: 'app-verify-email-backend',
  templateUrl: './verify-email-backend.component.html',
  styleUrls: ['./verify-email-backend.component.sass']
})
export class VerifyEmailBackendComponent implements OnInit {
  public errorMessage: string;
  public successMessage: string;
  pwReset: boolean;
  constructor( private activatedRoute: ActivatedRoute, private afService: AuthenticateServiceService) { 
    this.activatedRoute.queryParams.subscribe(params => {
      const code =  params['oobCode'];

      // check code
      this.afService.checkVerify(code)
      .then(res => {
        if (res.operation === 'PASSWORD_RESET') {
          this.pwReset = true;
        } else if (res.operation === 'VERIFY_EMAIL') {
          this.VerifyEmail(code);
        }

      })
      .catch(error => {
        this.errorMessage = error.message;
      });
  });
  }

  ngOnInit() {

  }

  private VerifyEmail(code: string) {
    this.afService.applyVerify(code)
    .then(() => {
      this.successMessage = 'Email verified!';
      this.afService.logout();
    })
    .catch(error => {
      this.errorMessage = error.message;
    });
  }

}
