import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VeryifyEmailComponent } from './veryify-email.component';

describe('VeryifyEmailComponent', () => {
  let component: VeryifyEmailComponent;
  let fixture: ComponentFixture<VeryifyEmailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VeryifyEmailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VeryifyEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
