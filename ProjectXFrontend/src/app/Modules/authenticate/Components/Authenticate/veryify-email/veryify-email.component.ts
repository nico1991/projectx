import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AuthenticateStateService } from 'src/app/Services/Authenticate/authenticate-state.service';
import { AuthenticateServiceService } from 'src/app/Services/Authenticate/authenticate-service.service';



@Component({
  selector: 'app-verify-email',
  templateUrl: './veryify-email.component.html',
  styleUrls: ['./veryify-email.component.sass']
})
export class VerifyEmailComponent implements OnInit {
  user$: Observable<firebase.User>;
  sended: boolean;
  errorMessage: string;
  constructor(private afstate: AuthenticateStateService, private afservice: AuthenticateServiceService) {
    this.user$ = afstate.getUser();
   }

  ngOnInit() {
  }

  sendVerification() {
    this.afstate.afAuth.authState.subscribe(user => {
      if (user) {
        this.afservice.sendVerification(user)
        .then(res => {
          this.sended = true;
        })
        .catch(error => {
          this.errorMessage =  error.message;
        });
      }

    });
  }
}
