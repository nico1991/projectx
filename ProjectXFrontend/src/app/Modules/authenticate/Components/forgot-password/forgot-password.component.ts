import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthenticateServiceService } from 'src/app/Services/Authenticate/authenticate-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticateStateService } from 'src/app/Services/Authenticate/authenticate-state.service';
import { DISABLED, FormControl } from '@angular/forms/src/model';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.sass']
})
export class ForgotPasswordComponent implements OnInit {
  EmailFormGroup: FormGroup;
  PasswordAgainFormGroup: FormGroup;
  PasswordFormGroup: FormGroup;
  hide = true;
  created = false;
  errorMessage: string;
  successMessage: string;
  code: string;
  constructor(
    private _formBuilder: FormBuilder,
    private authService: AuthenticateServiceService,
    private router: Router,
    private authstate: AuthenticateStateService,
    private activatedRoute: ActivatedRoute) {


    this.EmailFormGroup = this._formBuilder.group({
        // tslint:disable-next-line:max-line-length
      EmailCtrl: ['',  [Validators.required , Validators.pattern(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/)]]
    });

    this.PasswordAgainFormGroup = this._formBuilder.group({
      hideRequired: false,
      floatLabel: 'auto',
      PasswordAgainCtrl: ['', Validators.required, ]
    });

    this.PasswordFormGroup = this._formBuilder.group({
      hideRequired: false,
      floatLabel: 'auto',
// tslint:disable-next-line: max-line-length
      PasswordCtrl: ['', Validators.compose(  [Validators.required , Validators.pattern('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[$@$!%*?&])[A-Za-z\d$@$!%*?&].{8,}')])]
    });
  }

  ngOnInit() {
    // this.EmailFormGroup.disable();

    this.activatedRoute.queryParams.subscribe(params => {
      this.code =  params['oobCode'];
      this.authService.verifyPasswordReset(this.code)
      .then(email => {
        this.EmailFormGroup.setValue({EmailCtrl: email});

        // this.EmailFormGroup.disable();
      })
      .catch(error => {
        this.errorMessage = error;
      });
    });


  }

  match() {
    return this.PasswordFormGroup.value.PasswordCtrl === this.PasswordAgainFormGroup.value.PasswordAgainCtrl;
  }

  validCode() {
    return !this.errorMessage;
  }

// abc!!Abc11
  onApply() {
    this.authService.confirmPasswordReset(this.code, this.PasswordFormGroup.value.PasswordCtrl)
    .catch(error => {
      this.errorMessage = error.message;
    })
    .then(() => {
      this.router.navigate(['/authenticate/login']);
    });
  }
}
