import { NgModule } from '@angular/core';

import {LoginComponent} from './Components/Authenticate/login/login.component';
import {OpretComponent} from './Components/Authenticate/opret/opret.component';
import {VerifyEmailComponent} from './Components/Authenticate/veryify-email/veryify-email.component';

import { MaterialModule } from '../material/material.module';
import { CoreModule } from '../core/core.module';
import { AuthenticateRoutingModule } from './routing.module';

import { VerifyEmailBackendComponent } from './Components/Authenticate/verify-email-backend/verify-email-backend.component';
import { ForgotPasswordComponent } from './Components/forgot-password/forgot-password.component';


@NgModule({
  declarations: [
    LoginComponent,
    OpretComponent,
    VerifyEmailComponent,
    VerifyEmailBackendComponent,
    ForgotPasswordComponent,

  ],
  imports: [
    CoreModule,
    MaterialModule,
    AuthenticateRoutingModule,

  ]
})
export class AuthenticateModule { }
