import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './Components/Authenticate/login/login.component';
import { OpretComponent } from './Components/Authenticate/opret/opret.component';
import { VerifyEmailBackendComponent } from './Components/Authenticate/verify-email-backend/verify-email-backend.component';


const routes: Routes = [
    {path: 'login' , component: LoginComponent},
    {path: 'opret', component: OpretComponent},
    {path: '' , component: VerifyEmailBackendComponent},
  ];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class AuthenticateRoutingModule { }