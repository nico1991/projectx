import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs';
import { AuthenticateStateService } from 'src/app/Services/Authenticate/authenticate-state.service';
import { AuthenticateServiceService } from 'src/app/Services/Authenticate/authenticate-service.service';



@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.sass']
})
export class HomeComponent implements OnInit {
  user$: Observable<firebase.User>;

  constructor(
    public authState: AuthenticateStateService,
    private authService: AuthenticateServiceService) {
      this.user$ = authState.user.asObservable();
     }

  ngOnInit() {
  }

  logout() {
    this.authService.logout();
  }

}
