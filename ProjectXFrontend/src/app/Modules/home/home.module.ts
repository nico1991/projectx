import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import {HomeComponent} from './Components/home/home.component';

import { CoreModule } from '../core/core.module';
import { MaterialModule } from '../material/material.module';
import { HomeRoutingModule } from './routing.module';

@NgModule({
  declarations: [
    HomeComponent,
  ],
  imports: [
    CommonModule,
    CoreModule,
    MaterialModule,
    HomeRoutingModule
  ],

})
export class HomeModule { }
