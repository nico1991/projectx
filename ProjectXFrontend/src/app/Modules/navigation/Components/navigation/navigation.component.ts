import { Component, OnInit, ChangeDetectorRef } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable} from 'rxjs';
import { map } from 'rxjs/operators';

import { Router } from '@angular/router';
import { AuthenticateStateService } from 'src/app/Services/Authenticate/authenticate-state.service';
import { AuthenticateServiceService } from 'src/app/Services/Authenticate/authenticate-service.service';
import { AngularFireAuth } from '@angular/fire/auth';


@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css']
})
export class NavigationComponent implements OnInit {
  public user$: Observable<firebase.User>;
  isHandset$: Observable<boolean> = this.breakpointObserver.observe(Breakpoints.Handset)
    .pipe(
      map(result => result.matches)
    );


  constructor(
    private breakpointObserver: BreakpointObserver,
    private afstate: AuthenticateStateService,
    private afservice: AuthenticateServiceService,
    private router: Router) {



    }
    ngOnInit(): void {
      this.user$ = this.afstate.getUser();


    }

    logout() {
      this.afservice.logout();
      this.router.navigate(['']);
    }

}
