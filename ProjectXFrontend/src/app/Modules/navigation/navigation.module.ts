import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {NavigationComponent} from './Components/navigation/navigation.component';
import { CoreModule } from '../core/core.module';
import { MaterialModule } from '../material/material.module';
import { AuthenticateStateService } from 'src/app/Services/Authenticate/authenticate-state.service';
import { AuthenticateServiceService } from 'src/app/Services/Authenticate/authenticate-service.service';

@NgModule({
  declarations: [
    NavigationComponent,
  ],
  imports: [
    CommonModule,
    CoreModule,
    MaterialModule,
  ],

  exports: [
    NavigationComponent
  ]
})
export class NavigationModule { }
