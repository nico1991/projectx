import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AuthenticateServiceService {
  constructor(public afAuth: AngularFireAuth, private router: Router) {
    this.afAuth.auth.setPersistence('local');
  }

  signUp(email: string, password: string): Promise<firebase.auth.UserCredential> {
    return this.afAuth.auth.createUserWithEmailAndPassword(email, password);
  }

  refreshUser() {
    return this.afAuth.auth.currentUser.reload();
  }

  login(email: string, password: string): Promise<firebase.auth.UserCredential> {
    return this.afAuth.auth.signInWithEmailAndPassword(email, password);
  }
  logout(): Promise<void> {
    return this.afAuth.auth.signOut();
  }
  fortgotPassword(email: string) {
    return this.afAuth.auth.sendPasswordResetEmail(email);
  }
  sendVerification(user: firebase.User): Promise<void> {
    return user.sendEmailVerification();
  }

  checkVerify(code: string): Promise<firebase.auth.ActionCodeInfo> {
    return this.afAuth.auth.checkActionCode(code);
  }

  applyVerify(code: string): Promise<void> {
    return this.afAuth.auth.applyActionCode(code);
  }

  verifyPasswordReset(code: string): Promise<string> {
    return this.afAuth.auth.verifyPasswordResetCode(code);
  }

  confirmPasswordReset(code: string, newPW: string): Promise<void> {
    return this.afAuth.auth.confirmPasswordReset(code, newPW);
  }

}
