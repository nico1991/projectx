import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { Observable, BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthenticateStateService {
  public user = new BehaviorSubject<firebase.User>(null);

  constructor(public afAuth: AngularFireAuth) {
    this.user.next(JSON.parse( sessionStorage.getItem('AuthState'))) ;
    this.afAuth.user.subscribe(user => {
      if (user === null) {
        sessionStorage.clear();
        this.user.next(null);
      } else if (!user.emailVerified) {
        this.user.next(null);
      } else {
        this.user.next(user);
        sessionStorage.setItem('AuthState' , JSON.stringify( user ))
      }
    });

  }

  public getUser(): Observable<firebase.User> {
    return this.user.asObservable();
  }

  public verified(): boolean {
    return this.user.value.emailVerified;
  }
}
