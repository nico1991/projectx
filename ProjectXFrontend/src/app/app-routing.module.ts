import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PageNotFoundComponent } from './Modules/core/Components/page-not-found/page-not-found.component';
import { WelcomeComponent } from './Modules/core/welcome/welcome.component';
import { AuthGuard} from './Guard/AuthGuard';

import {AuthGuardCanLoad} from './Guard/AuthGuardCanLoad'


const routes: Routes = [
  // lazy
  {path: 'authenticate', loadChildren: './Modules/authenticate/authenticate.module#AuthenticateModule' },
  {path: 'home', loadChildren: './Modules/home/home.module#HomeModule', canLoad: [AuthGuardCanLoad]},
  // core
  {path: '' , component: WelcomeComponent},
  {path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
